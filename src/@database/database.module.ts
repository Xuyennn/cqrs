import { Module, Global } from '@nestjs/common';
import { MongoDBConnectProvider } from './mongodb/mongo.provider';

@Global()
@Module({
    providers : [
        ...MongoDBConnectProvider
    ],
    exports : [
        ...MongoDBConnectProvider
    ]
})
export class DatabaseModule {}
