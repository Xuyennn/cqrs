import { ConfigService } from './../../@config/config.service';
import { Provider, Inject } from "@nestjs/common";
import { MongoDbEnums } from "./mongodb.enum";
import * as mongoose from 'mongoose';

const mongoDBConfigs: mongoose.ConnectionOptions = {
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
};

export const MongoDBConnectProvider : Provider[] = [
    {
        provide : MongoDbEnums.Vehicle_Token,
        inject : [ConfigService],
        useFactory: async (configService: ConfigService): Promise<any> => {            
            return await mongoose.createConnection(                
                configService.get('MONGODB_URL'),                
                mongoDBConfigs             
            );
        }
    },

    {
        provide : MongoDbEnums.Vehicle_Command_Token,
        inject : [ConfigService],
        useFactory: async (configService: ConfigService): Promise<any> => {            
            return await mongoose.createConnection(                
                configService.get('MONGODB_URL'),                
                mongoDBConfigs             
            );
        }
    }
]