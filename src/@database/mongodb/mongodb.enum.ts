export enum MongoDbEnums {
    //query
    Vehicle_Token = 'Vehicle_token',
    Vehicle_Collection = 'Vehicles',

    //command
    Vehicle_Command_Token = 'Vehicle_command_token',
    Vehicle_Command_Collection = 'VehicleCommands'
}