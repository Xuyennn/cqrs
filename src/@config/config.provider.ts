import { ConfigService } from './config.service';
import { Provider } from '@nestjs/common';

export const ConfigServiceProvider : Provider = {
    provide: ConfigService,
    useFactory: (): ConfigService => {
        return ConfigService.getInstanceAsync(process.env.NODE_ENV)
    }
}