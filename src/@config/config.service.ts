import * as dotenv from 'dotenv';
import { readFileSync } from 'fs';
import { Injectable } from '@nestjs/common';
import { promises } from 'dns';

@Injectable()
export class ConfigService {
    private static instance: ConfigService = null;
    private static queues: Array<Object>;

    private constructor(filePath: string) {
        // const path = join(filePath);
        dotenv.config({ path: filePath });
        console.log(filePath);
    }

    static getInstanceAsync(envPath?: string): ConfigService {
        if (!this.instance) {
            const defaultEnvPath = `.env.${process.env.NODE_ENV}`;
            this.getEvents();
            console.log('4567');
            this.instance = new ConfigService(`.env.${envPath}` || defaultEnvPath);
        }

        return this.instance;
    }

    static getEvents(): void {
        const eventsManager = JSON.parse(readFileSync('events.json').toString());
        this.queues = eventsManager.queues.filter(queue => !!queue);
    }
    
    get(key: string): string {
        console.log(key);
        console.log(process.env[key]);
        return process.env[key];
    }
}