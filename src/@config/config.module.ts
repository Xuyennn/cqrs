import { Module, Global } from '@nestjs/common';
import { ConfigServiceProvider } from './config.provider';
import { ConfigService } from './config.service';

@Global()
@Module({
    providers : [
        ConfigServiceProvider
    ],
    exports : [
        ConfigService
    ]
})
export class ConfigModule {}
