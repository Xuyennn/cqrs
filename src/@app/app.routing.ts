import { VehicleModule } from '../@service/Vehicle.module';
import { Routes, RouterModule } from 'nest-router';
import { Module } from '@nestjs/common';

export const routes: Routes = [
    {
        path: 'Vehicle',
        module: VehicleModule
    }
];

@Module({
    imports: [
        RouterModule.forRoutes(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRouting { }