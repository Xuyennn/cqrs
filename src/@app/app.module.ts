import { VehicleModule } from '../@service/Vehicle.module';
import { DatabaseModule } from './../@database/database.module';
import { ConfigModule } from './../@config/config.module';
import { Module } from '@nestjs/common';
import { AppRouting } from './app.routing';


@Module({
  imports: [
    VehicleModule ,    
    ConfigModule,
    DatabaseModule,
    AppRouting
  ],
  controllers: [ ],
  providers: [ ],
})
export class AppModule {}
