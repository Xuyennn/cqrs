import {  VehicleRepository } from './repository/repo';
import { Inject, Injectable } from "@nestjs/common";
import { VehicleDocument } from './repository/documents/Vehicle.document';
import { CreateVehicle } from './Model/create-Vehicle.model';


@Injectable()
export class VehicleService {
    
    constructor(private vehicleRepository: VehicleRepository){

    }

    async get() :Promise<VehicleDocument[]> {
        return await this.vehicleRepository.getBM();
    }

    async createBM(createVehicle : CreateVehicle) :Promise<VehicleDocument>{
        return await this.vehicleRepository.createBM(createVehicle);
    }
}