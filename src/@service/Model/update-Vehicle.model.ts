import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, Max, Min } from "class-validator";

export class UpdateVehicle {
    id: string;
    
    @ApiProperty()
    @IsNotEmpty()              
    name: string;

    @ApiProperty()
    category: string;
    
    @ApiProperty()
    @Max(150)
    maxSpeed: number;

    @ApiProperty()
    @Min(3)
    @Max(10)
    gasTank: number;

    @ApiProperty()
    @Max(99999)
    distance: number;
  }