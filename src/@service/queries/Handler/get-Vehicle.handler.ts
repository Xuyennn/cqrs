import { QueryHandler, IQueryHandler } from "@nestjs/cqrs";
import { GetVehiclesQuery } from "../impl/get-vehicle.query";
import * as clc from 'cli-color';
import { VehicleRepository } from "src/@service/repository/repo";

@QueryHandler(GetVehiclesQuery)
export class GetVehiclesHandler implements IQueryHandler<GetVehiclesQuery> {
  constructor(private readonly repo: VehicleRepository) {}

  async execute(query: GetVehiclesQuery) {
    console.log(clc.yellowBright('Async get vehicle query ...'));
    return this.repo.getBM();
  }
}