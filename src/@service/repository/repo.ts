import { Model } from 'mongoose';
import { MongoDbEnums } from "src/@database/mongodb/mongodb.enum";
import { Inject, Injectable } from "@nestjs/common";
import {  IVehicleRepository } from './repo.interface';
import { Observable, from } from 'rxjs';
import { VehicleDocument } from './documents/Vehicle.document';
import { CreateVehicle } from '../Model/create-Vehicle.model';
import { VehicleCommandDocument } from './documents/Vehicle.Command.document';

@Injectable()
export class VehicleRepository implements IVehicleRepository {
    constructor(
        @Inject(MongoDbEnums.Vehicle_Collection)
        private repoQuery: Model<VehicleDocument>,
        @Inject(MongoDbEnums.Vehicle_Command_Collection)
        private repoCommand: Model<VehicleCommandDocument>
    ) { }

    //#region  3 layer
    async createBM(para : CreateVehicle) : Promise<VehicleDocument> {        
        return await this.repoQuery.create(para);
    }
    async getBM() : Promise<VehicleDocument[]> {
        return await this.repoQuery.find();
    }
    //#endregion

    //#region  cqrs query
    getVehicle(): Observable<VehicleDocument[]> {
        return null;
    }

    async getVehicleById(vehicleid: string): Promise<VehicleDocument> {
        return await this.repoQuery.findById(vehicleid);
    }

    createQueryVehicle(para : CreateVehicle): Promise<VehicleDocument> {
        return this.repoQuery.create(para) ;
    }
    //#endregion 

    //#region command
    createCommandVehicle (para : any) {
        return  this.repoCommand.create(para);
     }
    //#endregion
}