import { CreateVehicle } from 'src/@service/Model/create-Vehicle.model';
import { Observable } from "rxjs";
import { VehicleDocument } from "./documents/Vehicle.document";



export abstract class IVehicleRepository {
    // Query
    getVehicle(): Observable<VehicleDocument[]> {
        return null
    }

    getVehicleById(vehicle: string): Promise<VehicleDocument> {
        return null
    }

    createQueryVehicle(para : CreateVehicle): any {
        return null
    }

    //command
    createCommandVehicle (para : any) {
       return null ; 
    }
}