import { v4 } from 'uuid';
import { Schema } from 'mongoose';

export const VehicleCommandSchema = new Schema({
    _id: { type: String, default: v4 },
    eventName: { type: String },
    date: { type: String },
    streamId: { type: String },
    payload: { type: Object }
});