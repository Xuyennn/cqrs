import { Schema } from 'mongoose';
import { v4 } from 'uuid';

export const VehicleSchema = new Schema({
         _id: { type: String, default: v4 },
         name: { type: String, default: 'Guest' },
         category: { type: String, default: 'type 1' },
         status: { type: String, default: 'active' },
         maxSpeed: { type: Number, default: 0 },
         gasTank: { type: Number, default: 3 },
         distance: { type: Number, default: 0 },
       });
