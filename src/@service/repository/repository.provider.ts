import { VehicleCommandSchema } from './Schemas/Vehicle.Command.Schema';
import { Provider } from "@nestjs/common";
import { MongoDbEnums } from "src/@database/mongodb/mongodb.enum"
import { Connection, Model } from 'mongoose';

import { IVehicleRepository } from "./repo.interface";
import {  VehicleRepository } from "./repo";
import { VehicleDocument } from "./documents/Vehicle.document";
import { VehicleSchema } from "./Schemas/Vehicle.schema";


export const VehicleCollectionProvider : Provider[] = [
    {
        provide: MongoDbEnums.Vehicle_Collection,
        useFactory: (connection: Connection): Model<VehicleDocument, {}> => {
            return connection.model(
                MongoDbEnums.Vehicle_Collection,
                VehicleSchema
            );
        },
        inject: [MongoDbEnums.Vehicle_Token]
    },

    {
        provide: MongoDbEnums.Vehicle_Command_Collection,
        useFactory: (connection: Connection): Model<VehicleDocument, {}> => {
            return connection.model(
                MongoDbEnums.Vehicle_Command_Collection,
                VehicleCommandSchema
            );
        },
        inject: [MongoDbEnums.Vehicle_Token]
    }
];

export const VehicleRepositoryProvider: Provider = {
    provide: IVehicleRepository,
    useClass: VehicleRepository,
    inject: [MongoDbEnums.Vehicle_Collection , MongoDbEnums.Vehicle_Command_Collection]
}