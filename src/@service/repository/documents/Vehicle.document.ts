export class VehicleDocument extends Document {    
  name: string;
  category: string;
  status: string;
  maxSpeed: number;
  gasTank: number;
  distance: number;
}