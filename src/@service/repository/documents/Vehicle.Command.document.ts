export interface VehicleCommandDocument extends Document {    
    eventName: string,
    date: string ,
    streamId:  string ,
    payload:  object
}