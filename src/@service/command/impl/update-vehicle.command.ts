import { UpdateVehicle } from './../../Model/update-Vehicle.model';
export class UpdateVehicleCommand {
    constructor(
        public updateModel : UpdateVehicle
    ){

    }
}