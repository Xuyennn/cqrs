import { CreateVehicle } from "src/@service/Model/create-Vehicle.model";

export class CreateVehicleCommand {
    constructor (
        public createModel : CreateVehicle
    ){}
}