import { CreateVehicleHandler } from "./create-Vehicle.handler";
import { UpdateVehicleHandler } from "./update-Vehicle.handler";

export const VehicleCommandHandler = [
    CreateVehicleHandler , 
    UpdateVehicleHandler
]