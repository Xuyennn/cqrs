import { UpdateVehicleCommand } from "../impl/update-vehicle.command";
import { CommandHandler, ICommandHandler } from "@nestjs/cqrs";
import { Inject } from "@nestjs/common";
import { IVehicleRepository } from "src/@service/repository/repo.interface";
import * as clc from 'cli-color';


@CommandHandler(UpdateVehicleCommand)
export class UpdateVehicleHandler implements ICommandHandler<UpdateVehicleCommand> {
  constructor(
    // private readonly repository: HeroRepository,
    // private readonly publisher: EventPublisher,
    // private readonly eventBus : EventBus
    @Inject(IVehicleRepository.name)
    private repo : IVehicleRepository
  ) {}

  async execute(command: UpdateVehicleCommand) {
    console.log(clc.greenBright('Came Update CommandHandler...'));
    const updatemodel = command.updateModel;

    const vehicle = await this.repo.getVehicleById(updatemodel.id);
    
  }
}