export enum VehicleCommandName {
    Create_Vehicle = 'CreateVehicle' ,
    Update_Vehicle = 'UpdateVehicle' ,
    Delete_Vehicle = 'DeleteVehicle'
}