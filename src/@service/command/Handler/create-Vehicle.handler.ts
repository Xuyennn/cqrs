import { CreateVehicleCommand } from "../impl/create-Vehicle.command";
import { ICommandHandler, CommandHandler, EventBus } from "@nestjs/cqrs";
import * as clc from 'cli-color';
import { Inject } from "@nestjs/common";
import { IVehicleRepository } from "src/@service/repository/repo.interface";
import { VehicleCommandDocument } from "src/@service/repository/documents/Vehicle.Command.document";
import { VehicleCommandName } from "./command.enum";

@CommandHandler(CreateVehicleCommand)
export class CreateVehicleHandler implements ICommandHandler<CreateVehicleCommand> {
  constructor(
    // private readonly repository: HeroRepository,
    // private readonly publisher: EventPublisher,
    // private readonly eventBus : EventBus
    @Inject(IVehicleRepository.name)
    private repo : IVehicleRepository
  ) {}

  async execute(command: CreateVehicleCommand) {
    console.log(clc.greenBright('Came Create CommandHandler...'));
    // const { heroId, dragonId } = command;
    // const hero = this.publisher.mergeObjectContext(
    //   await this.repository.findOneById(+heroId),
    // );
    // hero.killEnemy(dragonId);
    // hero.commit();
    const payload = command.createModel ;    
    console.log(payload);

    // create for query then add 1 es
    const vehicle = await this.repo.createQueryVehicle(payload);
    
    console.log(vehicle);
    // tai sao cho nay phai can partial
    const commandEs : Partial<VehicleCommandDocument> = {
      eventName: VehicleCommandName.Create_Vehicle,
      date: new Date().toISOString() , 
      streamId:  vehicle._id ,
      payload:  payload
    }

    await this.repo.createCommandVehicle(commandEs);

    return  vehicle;
  }
}