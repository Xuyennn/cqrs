import { EventsHandler, IEventHandler } from "@nestjs/cqrs";
import { CreatedVehicleEvent } from "../impl/create-vehicle";
import * as clc from 'cli-color';

@EventsHandler(CreatedVehicleEvent)
export class HeroKilledDragonHandler implements IEventHandler<CreatedVehicleEvent> {
  handle(event: CreatedVehicleEvent) {
    console.log(clc.greenBright('HeroKilledDragonEvent...'));
  }
}