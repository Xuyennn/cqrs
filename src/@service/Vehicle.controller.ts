import { Controller, Get, Post, Body, Put, Param } from "@nestjs/common";
import {  VehicleService } from "./Vehicle.service";
import { VehicleDocument } from "./repository/documents/Vehicle.document";
import { CommandBus, QueryBus } from "@nestjs/cqrs";
import { GetVehiclesQuery } from "./queries/impl/get-vehicle.query";
import { CreateVehicle } from "./Model/create-Vehicle.model";
import { CreateVehicleCommand } from "./command/impl/create-Vehicle.command";
import { UpdateVehicle } from "./Model/update-Vehicle.model";
import { ApiParam } from "@nestjs/swagger";
import { UpdateVehicleCommand } from "./command/impl/update-vehicle.command";

@Controller()
export class VehicleController {
  constructor(
    private vehiclesv: VehicleService,
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  //#region 3layer normal
//   @Get()
//   async getBm(): Promise<VehicleDocument[]> {
//     return await this.vehiclesv.get();
//   }

//   @Post()
//   async createVihicle(@Body() para: CreateVehicle): Promise<VehicleDocument> {
//     return await this.vehiclesv.createBM(para);
//   }
  ////#endregion


  //#region cqrs
  @Get()
  async findAll(): Promise<VehicleDocument[]> {
    return await this.queryBus.execute(new GetVehiclesQuery());
  }

  @Post()
  async createVihicle(@Body() para: CreateVehicle): Promise<VehicleDocument> {    
    return await this.commandBus.execute(new CreateVehicleCommand(para));
  }

  @ApiParam({name : 'id'})
  @Put(':id')
  async updateVehicle(@Param('id') id :string , @Body() para : UpdateVehicle) : Promise<VehicleDocument> {
    console.log(id);
    para.id = id ;
    return await this.commandBus.execute(new UpdateVehicleCommand(para));
    
  }

  ////#endregion
}