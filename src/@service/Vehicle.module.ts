import { Module } from '@nestjs/common';
import {  VehicleService } from './Vehicle.service';
import { CqrsModule } from '@nestjs/cqrs';
import {  VehicleRepository } from './repository/repo';
import {  VehicleRepositoryProvider, VehicleCollectionProvider } from './repository/repository.provider';
import { VehicleController } from './Vehicle.controller';
import { QueryHandlers } from './queries/Handler';
import { VehicleCommandHandler } from './command/Handler';


@Module({
    imports : [
         CqrsModule
    ],
    controllers :[ 
        VehicleController
    ],
    providers : [
        ...VehicleCollectionProvider,
        VehicleService,
        VehicleRepositoryProvider,
        VehicleRepository,
        ...QueryHandlers,
        ...VehicleCommandHandler
    ]
})
export class VehicleModule {}
