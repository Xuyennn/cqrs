import { NestFactory } from '@nestjs/core';
import { AppModule } from './@app/app.module';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';
import { ConfigService } from './@config/config.service';

async function bootstrap() {    

  const app = await NestFactory.create(AppModule);
    
  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle('vehicle APIs')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('apis', app, document); //para 1 trung ten controller thi controller vo hieu

  await app.listen(process.env.PORT || 3000);
}
bootstrap();
